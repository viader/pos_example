package pl.fewbits.pos.app.barcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import org.junit.Test;

public class BarcodeScannerTest {

  @Test
  public void scanShortCode() {
    String code = "Lorem ipsum";
    assertThat(scanBarcode(code)).isEqualTo(code);
  }

  @Test
  public void scanLongCode() {
    String code = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
        "\nDuis ullamcorper libero felis, vel mattis augue accumsan vel. Aenean vitae viverra augue, at molestie lorem.";
    assertThat(scanBarcode(code)).isEqualTo(code);
  }

  @Test
  public void scanPolishCharactersCode() {
    String code = "Zażółć gęślą jaźń.";
    assertThat(scanBarcode(code)).isEqualTo(code);
  }

  private String scanBarcode(String data) {
    return scanBarcode(data.getBytes(StandardCharsets.UTF_8));
  }

  private String scanBarcode(byte[] data) {
    BarcodeScanner scanner = new BarcodeScanner();
    return scanner.scanBardcode(data);
  }
}
