package pl.fewbits.pos.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.reactivex.annotations.NonNull;
import java.nio.charset.StandardCharsets;
import org.joda.money.Money;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.fewbits.pos.app.barcode.BarcodeScanner;
import pl.fewbits.pos.app.database.ProductRepository;
import pl.fewbits.pos.app.domain.Product;
import pl.fewbits.pos.app.domain.Receipt;
import pl.fewbits.pos.app.output.LcdDisplay;
import pl.fewbits.pos.app.output.Printer;
import pl.fewbits.pos.core.C;
import pl.fewbits.pos.core.Currency;
import pl.fewbits.pos.input.domain.InputEvent;

@RunWith(MockitoJUnitRunner.class)
public class PointOfSaleTest {

  private PointOfSale pointOfSale;

  @Mock
  private ProductRepository productRepository;

  @Mock
  private LcdDisplay lcdDisplay;

  @Mock
  private Printer printer;

  @Mock
  private BarcodeScanner barcodeScanner;

  @Before
  public void setUp() {
    pointOfSale = Mockito.spy(new PointOfSale(productRepository, lcdDisplay, printer, barcodeScanner));
  }

  @Test
  public void consumeGoodBarcode() {
    String productCode = "chleb";
    InputEvent inputEvent = createInputEvent(productCode);

    when(barcodeScanner.isValid(inputEvent.getScannedData())).thenReturn(true);
    when(barcodeScanner.scanBardcode(inputEvent.getScannedData())).thenReturn(productCode);
    pointOfSale.consume(inputEvent);

    verify(barcodeScanner, Mockito.atLeastOnce()).isValid(inputEvent.getScannedData());
    verify(barcodeScanner, Mockito.atLeastOnce()).scanBardcode(inputEvent.getScannedData());
    verify(pointOfSale, Mockito.atLeastOnce()).onScannedCode(productCode);
    verify(pointOfSale, Mockito.never()).onInvalidBarCode();
  }

  @Test
  public void consumeBadBarcode() {
    InputEvent inputEvent = createInputEvent(anyString());

    pointOfSale.consume(inputEvent);

    verify(pointOfSale, Mockito.atLeastOnce()).onInvalidBarCode();
    verify(pointOfSale, Mockito.never()).onScannedCode(anyString());
  }

  @Test
  public void onScannedCodeProduct() {
    String code = "chleb";

    pointOfSale.onScannedCode(code);

    verify(pointOfSale, Mockito.atLeastOnce()).onScannedProductCode(code);
  }

  @Test
  public void onScannedCodeExit() {
    pointOfSale.onScannedCode(C.System.EXIT_CODE);

    verify(pointOfSale, Mockito.atLeastOnce()).finishTransaction();
  }

  @Test
  public void productsFound() {
    PointOfSale pointOfSale = new PointOfSale(productRepository, lcdDisplay, printer, barcodeScanner);
    String breadCode = "breadCode";
    String butterCode = "butterCode";
    Product bread = new Product("bread", "bread", Money.of(Currency.PLN, 1));
    Product butter = new Product("butter", "butter", Money.of(Currency.PLN, 2));

    when(productRepository.findProduct(breadCode)).thenReturn(bread);
    when(productRepository.findProduct(butterCode)).thenReturn(butter);
    pointOfSale.onScannedProductCode(breadCode);
    pointOfSale.onScannedProductCode(butterCode);

    assertThat(pointOfSale.getCurrentReceipt().getProducts()).containsOnly(bread, butter);
    verify(lcdDisplay, Mockito.atLeast(2)).display(any(Product.class));
  }

  @Test
  public void productsNotFound() {
    PointOfSale pointOfSale = new PointOfSale(productRepository, lcdDisplay, printer, barcodeScanner);
    String breadCode = "breadCode";
    String butterCode = "butterCode";

    when(productRepository.findProduct(breadCode)).thenReturn(null);
    when(productRepository.findProduct(butterCode)).thenReturn(null);
    pointOfSale.onScannedProductCode(breadCode);
    pointOfSale.onScannedProductCode(butterCode);

    assertThat(pointOfSale.getCurrentReceipt().getProducts()).isEmpty();
    verify(lcdDisplay, timeout(5000).times(2)).display(C.UserMessages.LCD_PRODUCT_NOT_FOUND);
  }

  @Test
  public void productsFoundAndExit() {
    PointOfSale pointOfSale = new PointOfSale(productRepository, lcdDisplay, printer, barcodeScanner);
    String breadCode = "breadCode";
    String butterCode = "butterCode";
    Product bread = new Product("bread", "bread", Money.of(Currency.PLN, 1));
    Product butter = new Product("butter", "butter", Money.of(Currency.PLN, 2));

    when(productRepository.findProduct(breadCode)).thenReturn(bread);
    when(productRepository.findProduct(butterCode)).thenReturn(butter);
    pointOfSale.onScannedProductCode(breadCode);
    pointOfSale.onScannedProductCode(butterCode);
    pointOfSale.onScannedCode(C.System.EXIT_CODE);

    assertThat(pointOfSale.getCurrentReceipt().getProducts()).isEmpty();
    verify(lcdDisplay, timeout(5000).times(2)).display(any(Product.class));
    verify(lcdDisplay, timeout(5000).times(1)).displayReceiptPrice(any(Receipt.class));
    verify(printer, timeout(5000).times(1)).print(any(Receipt.class));
  }

  private InputEvent createInputEvent(@NonNull String scannedData) {
    return new InputEvent(scannedData.getBytes(StandardCharsets.UTF_8));
  }
}
