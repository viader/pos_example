package pl.fewbits.pos.app.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import org.joda.money.Money;
import org.junit.Test;
import pl.fewbits.pos.core.Currency;

public class ReceiptTest {

  @Test
  public void initTotalSumZero() {
    Receipt receipt = new Receipt();
    assertThat(receipt.getTotalPrice().getAmount()).isZero();
  }

  @Test
  public void totalBigTotalSum() {
    BigDecimal carPrice = new BigDecimal(942241000);
    BigDecimal housePrice = new BigDecimal(233000);
    BigDecimal catPrice = new BigDecimal("123432453252345235325325325235324324142142143252352");

    Receipt receipt = new Receipt();
    receipt.getProducts().add(new Product("car", "car", Money.of(Currency.PLN, carPrice)));
    receipt.getProducts().add(new Product("house", "house", Money.of(Currency.PLN, housePrice)));
    receipt.getProducts().add(new Product("cat", "cat", Money.of(Currency.PLN, catPrice)));


    assertThat(receipt.getTotalPrice().getAmount()).isEqualByComparingTo(carPrice.add(housePrice).add(catPrice));
  }
}
