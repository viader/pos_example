package pl.fewbits.pos.app.database;

import io.reactivex.annotations.Nullable;
import java.util.HashMap;
import java.util.Map;
import org.joda.money.Money;
import pl.fewbits.pos.app.domain.Product;
import pl.fewbits.pos.core.Currency;

public class ProductRepository {

  private static final int DATABASE_DELAY_MS = 2000;

  private final Map<String, Product> productsMap = new HashMap<>();

  {
    productsMap.put("chleb", new Product("chleb", "Chleb razowy", Money.of(Currency.PLN, 1.5)));
    productsMap.put("mleko", new Product("chleb", "Mleko 2%", Money.of(Currency.PLN, 2.2)));
    productsMap.put("maka", new Product("chleb", "Mąka", Money.of(Currency.PLN, 4.0)));
    productsMap.put("woda", new Product("chleb", "Woda mineralna", Money.of(Currency.PLN, 1.0)));
    productsMap.put("kurczak", new Product("chleb", "Kurczak 500g", Money.of(Currency.PLN, 7)));
    productsMap.put("baton", new Product("chleb", "Baton czekoladowy", Money.of(Currency.PLN, 2.18)));
  }

  @Nullable
  public Product findProduct(String productCode) {
    delay();
    if (productsMap.containsKey(productCode)) {
      return productsMap.get(productCode);
    }
    return null;
  }

  private void delay() {
    try {
      Thread.sleep(DATABASE_DELAY_MS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
