package pl.fewbits.pos.app.barcode;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import java.nio.charset.StandardCharsets;

public class BarcodeScanner {

  @NonNull
  public String scanBardcode(@NonNull byte[] barcodeData) {
    return new String(barcodeData, StandardCharsets.UTF_8);
  }

  public boolean isValid(@Nullable byte[] barcodeData) {
    return barcodeData != null && barcodeData.length > 0;
  }
}
