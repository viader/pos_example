package pl.fewbits.pos.app;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import java.util.Optional;
import javax.inject.Inject;
import lombok.NonNull;
import pl.fewbits.pos.app.barcode.BarcodeScanner;
import pl.fewbits.pos.app.database.ProductRepository;
import pl.fewbits.pos.app.domain.Product;
import pl.fewbits.pos.app.domain.Receipt;
import pl.fewbits.pos.app.output.LcdDisplay;
import pl.fewbits.pos.app.output.Printer;
import pl.fewbits.pos.core.C;
import pl.fewbits.pos.input.ConsumerInputEvent;
import pl.fewbits.pos.input.domain.InputEvent;

public class PointOfSale implements ConsumerInputEvent {

  private ProductRepository productRepository;

  private LcdDisplay lcdDisplay;

  private Printer printer;

  private BarcodeScanner barcodeScanner;

  private Receipt currentReceipt = new Receipt();

  @Inject
  public PointOfSale(ProductRepository productRepository, LcdDisplay lcdDisplay, Printer printer, BarcodeScanner barcodeScanner) {
    this.productRepository = productRepository;
    this.lcdDisplay = lcdDisplay;
    this.printer = printer;
    this.barcodeScanner = barcodeScanner;
  }

  public void finishTransaction() {
    lcdDisplay.displayReceiptPrice(currentReceipt);
    printer.print(currentReceipt);
    currentReceipt = new Receipt();
  }

  @Override
  public void consume(InputEvent inputEvent) {
    if (barcodeScanner.isValid(inputEvent.getScannedData())) {
      onScannedCode(barcodeScanner.scanBardcode(inputEvent.getScannedData()));
    } else {
      onInvalidBarCode();
    }
  }

  public void onScannedCode(@NonNull String code) {
    if (code.equals(C.System.EXIT_CODE)) {
      finishTransaction();
    } else {
      onScannedProductCode(code);
    }
  }

  public void onInvalidBarCode() {
    lcdDisplay.display(C.UserMessages.LCD_INVALID_BAR_CODE);
  }

  public void onScannedProductCode(@NonNull String productCode) {
    Observable.fromCallable(() -> {
      return Optional.ofNullable(productRepository.findProduct(productCode));
    })
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.computation())
        .subscribe(productOptional -> {
          if (productOptional.isPresent()) {
            onProductFound(productOptional.get());
          } else {
            onProductNotFound();
          }
        });
  }

  private void onProductFound(Product product) {
    lcdDisplay.display(product);
    currentReceipt.getProducts().add(product);
  }

  private void onProductNotFound() {
    lcdDisplay.display(C.UserMessages.LCD_PRODUCT_NOT_FOUND);
  }

  public Receipt getCurrentReceipt() {
    return currentReceipt;
  }
}
