package pl.fewbits.pos.app.output;

import pl.fewbits.pos.app.domain.Receipt;
import pl.fewbits.pos.core.C;
import pl.fewbits.pos.core.util.AppMoneyFormatter;

public class Printer {

  private static AppMoneyFormatter moneyFormatter = new AppMoneyFormatter();

  public void print(Receipt receipt) {
    print(generatePage(receipt));
  }

  public void print(String page) {
    System.out.println(C.UserMessages.DEVICE_SEPARATOR);
    System.out.println(C.UserMessages.PRINTER);
    System.out.println(page);
    System.out.println(C.UserMessages.DEVICE_SEPARATOR);
  }

  public static String generatePage(Receipt receipt) {
    StringBuilder builder = new StringBuilder();
    receipt.getProducts().forEach(p -> {
      builder.append(String.format(C.UserMessages.PRINTER_RECEIPT_PRODUCT_ROW, p.getName(), moneyFormatter.format(p.getPrice())));
      builder.append(C.Text.NEW_LINE);
    });
    builder.append(String.format(C.UserMessages.PRINTER_RECEIPT_PRICE, moneyFormatter.format(receipt.getTotalPrice())));
    return builder.toString();
  }
}
