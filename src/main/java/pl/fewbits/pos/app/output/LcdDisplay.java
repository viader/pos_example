package pl.fewbits.pos.app.output;

import pl.fewbits.pos.app.domain.Product;
import pl.fewbits.pos.app.domain.Receipt;
import pl.fewbits.pos.core.C;
import pl.fewbits.pos.core.util.AppMoneyFormatter;

public class LcdDisplay {

  private static AppMoneyFormatter moneyFormatter = new AppMoneyFormatter();

  public void display(Product product) {
    display(generateProductFoundMessage(product));
  }

  public void displayReceiptPrice(Receipt receipt) {
    display(String.format(C.UserMessages.LCD_RECEIPT_PRICE, moneyFormatter.format(receipt.getTotalPrice())));
  }

  public void display(String message) {
    System.out.println(C.UserMessages.DEVICE_SEPARATOR);
    System.out.println(C.UserMessages.LCD);
    System.out.println(message);
    System.out.println(C.UserMessages.DEVICE_SEPARATOR);
  }

  public static String generateProductFoundMessage(Product product) {
    return String.format(C.UserMessages.LCD_PRODUCT_FOUND, product.getName(), moneyFormatter.format(product.getPrice()));
  }
}
