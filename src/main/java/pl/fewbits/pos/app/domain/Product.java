package pl.fewbits.pos.app.domain;

import lombok.NonNull;
import lombok.Value;
import org.joda.money.Money;

@Value
public class Product {

  @NonNull
  String productCode;

  @NonNull
  String name;

  @NonNull
  Money price;
}
