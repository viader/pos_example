package pl.fewbits.pos.app.domain;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import pl.fewbits.pos.core.C;

@Data
public class Receipt {

  List<Product> products = new ArrayList<>();

  public Money getTotalPrice() {
    Money money = Money.of(CurrencyUnit.getInstance(C.Currency.PLN), 0);
    for(Product product : products) {
      money = money.plus(product.getPrice());
    }
    return money;
  }
}
