package pl.fewbits.pos;

import pl.fewbits.pos.core.di.DaggerRootComponent;
import pl.fewbits.pos.core.di.RootComponent;

public final class Application {

  public static void main(String... args) throws InterruptedException {
    System.out.println("PointOfSale start!");
    System.out.println("Please type product code:");

    RootComponent rootComponent = DaggerRootComponent.create();
    rootComponent.getConsumerInputEventThread().start();
    rootComponent.getProducerInputEventThread().start();
  }
}
