package pl.fewbits.pos.core.di;

import dagger.Module;
import dagger.Provides;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.inject.Named;
import javax.inject.Singleton;
import pl.fewbits.pos.app.PointOfSale;
import pl.fewbits.pos.app.barcode.BarcodeScanner;
import pl.fewbits.pos.app.database.ProductRepository;
import pl.fewbits.pos.app.output.LcdDisplay;
import pl.fewbits.pos.app.output.Printer;
import pl.fewbits.pos.core.C;
import pl.fewbits.pos.input.ConsumerInputEventThread;
import pl.fewbits.pos.input.ProducerInputEventThread;
import pl.fewbits.pos.input.domain.InputEvent;

@Module
public class RootModule {

  @Provides
  @Singleton
  public ProductRepository provideProductRepository() {
    return new ProductRepository();
  }

  @Provides
  @Singleton
  public LcdDisplay provideLcdDisplay() {
    return new LcdDisplay();
  }

  @Provides
  @Singleton
  public Printer providePrinter() {
    return new Printer();
  }

  @Provides
  @Singleton
  public BarcodeScanner provideBardcodeScanner() {
    return new BarcodeScanner();
  }

  @Provides
  @Singleton
  @Named(C.InjectionDependency.INPUT_EVENTS_QUEUE)
  public BlockingQueue<InputEvent> provideInputEventsQueue() {
    return new LinkedBlockingQueue<>();
  }

  @Provides
  @Singleton
  public ProducerInputEventThread provideProducerInputEventThread(@Named(C.InjectionDependency.INPUT_EVENTS_QUEUE) BlockingQueue<InputEvent> queue) {
    return new ProducerInputEventThread(queue);
  }

  @Provides
  @Singleton
  public ConsumerInputEventThread provideConsumerInputEventThread(@Named(C.InjectionDependency.INPUT_EVENTS_QUEUE) BlockingQueue<InputEvent> queue,
                                                                  PointOfSale pointOfSale) {
    return new ConsumerInputEventThread(queue, pointOfSale);
  }
}
