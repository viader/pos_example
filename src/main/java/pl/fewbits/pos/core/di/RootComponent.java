package pl.fewbits.pos.core.di;

import dagger.Component;
import javax.inject.Singleton;
import pl.fewbits.pos.input.ConsumerInputEventThread;
import pl.fewbits.pos.input.ProducerInputEventThread;

@Singleton
@Component(modules = RootModule.class)
public interface RootComponent {

  ProducerInputEventThread getProducerInputEventThread();

  ConsumerInputEventThread getConsumerInputEventThread();
}
