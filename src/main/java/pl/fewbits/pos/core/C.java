package pl.fewbits.pos.core;

import java.util.Collections;
import java.util.List;

public interface C {

  interface Text {
    String SPACE = " ";
    String NEW_LINE = java.lang.System.lineSeparator();
  }

  interface System {
    String EXIT_CODE = "exit";
  }

  interface Currency {
    String PLN = "PLN";
    String SYMBOL = "zł";
    int NUMERIC_CODE = 0;
    int DECIMAL_PLACES = 2;
    List<String> COUNTRY_CODES = Collections.singletonList("PLN");
  }

  interface InjectionDependency {
    String INPUT_EVENTS_QUEUE = "INPUT_EVENTS_QUEUE";
  }

  interface UserMessages {
    String DEVICE_SEPARATOR = "##########################";
    String LCD = "LCD display";
    String LCD_PRODUCT_FOUND = "Product %s, Price %s";
    String LCD_PRODUCT_NOT_FOUND = "Product not found";
    String LCD_INVALID_BAR_CODE = "Invalid bar-code";
    String LCD_RECEIPT_PRICE = "Total price: %s";
    String PRINTER = "Printer";
    String PRINTER_RECEIPT_PRODUCT_ROW = "Product %s, Price %s";
    String PRINTER_RECEIPT_PRICE = "Total price: %s";
  }
}
