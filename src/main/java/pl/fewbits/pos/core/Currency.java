package pl.fewbits.pos.core;

import static pl.fewbits.pos.core.C.Currency.COUNTRY_CODES;

import org.joda.money.CurrencyUnit;
import org.joda.money.IllegalCurrencyException;

final public class Currency {

  public static CurrencyUnit PLN;

  static {
    try {
      PLN = CurrencyUnit.of(C.Currency.PLN);
    } catch (IllegalCurrencyException e) {
      PLN = CurrencyUnit.registerCurrency(C.Currency.PLN, C.Currency.NUMERIC_CODE, C.Currency.DECIMAL_PLACES, COUNTRY_CODES);
    }
  }
}
