package pl.fewbits.pos.core.util;

import org.joda.money.Money;
import org.joda.money.format.MoneyFormatter;
import org.joda.money.format.MoneyFormatterBuilder;
import pl.fewbits.pos.core.C;

public class AppMoneyFormatter {

  private MoneyFormatter jodaFormatter;

  public AppMoneyFormatter() {
    MoneyFormatterBuilder builder = new MoneyFormatterBuilder();
    jodaFormatter = builder.appendAmount().appendLiteral(C.Text.SPACE).appendCurrencyCode().toFormatter();
  }

  public String format(Money money) {
    return jodaFormatter.print(money);
  }
}
