package pl.fewbits.pos.core.util;

public class TextUtils {

  public static boolean isEmpty(String text) {
    return text != null && text.isEmpty();
  }
}
