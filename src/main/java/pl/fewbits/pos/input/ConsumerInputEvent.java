package pl.fewbits.pos.input;

import pl.fewbits.pos.input.domain.InputEvent;

public interface ConsumerInputEvent {

  void consume(InputEvent inputEvent);
}
