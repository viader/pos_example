package pl.fewbits.pos.input;

import io.reactivex.annotations.NonNull;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import pl.fewbits.pos.core.util.TextUtils;
import pl.fewbits.pos.input.domain.InputEvent;

public class ProducerInputEventThread extends Thread {

  private static String TAG = "ProducerInputEventThread";

  @NonNull
  private BlockingQueue<InputEvent> inputEvents;

  private Scanner inputReader;

  public ProducerInputEventThread(@NonNull BlockingQueue<InputEvent> inputEvents) {
    super(TAG);
    this.inputEvents = inputEvents;
    inputReader = new Scanner(System.in);
  }

  @Override
  public void run() {
    while (true) {
      if (inputReader.hasNextLine()) {
        String scannedData = inputReader.nextLine();
        byte[] result = new byte[0];
        if (!TextUtils.isEmpty(scannedData)) {
          result = scannedData.getBytes(StandardCharsets.UTF_8);
        }
        emitEvent(new InputEvent(result));
      }
    }
  }

  private void emitEvent(InputEvent inputEvent) {
    try {
      inputEvents.put(inputEvent);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
