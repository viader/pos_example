package pl.fewbits.pos.input.domain;

import lombok.NonNull;
import lombok.Value;

@Value
public class InputEvent {

  @NonNull
  byte[] scannedData;
}
