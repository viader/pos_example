package pl.fewbits.pos.input;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import java.util.concurrent.BlockingQueue;
import pl.fewbits.pos.input.domain.InputEvent;

public class ConsumerInputEventThread extends Thread {

  private static String TAG = "ConsumerInputEventThread";

  @NonNull
  private BlockingQueue<InputEvent> inputEvents;

  @NonNull
  private ConsumerInputEvent consumerInputEvent;

  public ConsumerInputEventThread(@NonNull BlockingQueue<InputEvent> inputEvents, @NonNull ConsumerInputEvent consumerInputEvent) {
    super(TAG);
    this.inputEvents = inputEvents;
    this.consumerInputEvent = consumerInputEvent;
  }

  public void run() {
    while (true) {
      InputEvent inputEvent = waitAndGetEvent();
      if (inputEvent != null) {
        consumerInputEvent.consume(inputEvent);
      }
    }
  }

  @Nullable
  private InputEvent waitAndGetEvent() {
    InputEvent inputEvent = null;
    try {
      inputEvent = inputEvents.take();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return inputEvent;
  }
}
