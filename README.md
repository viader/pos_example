Point of Sale - example project for recruitment process. Edited[1]

Task: Implements a single point of sale.

Assume you have:  
- one input device: bar code scanner  
- two output devices: LCD display and printer  

Implements:  
- single product sale: products bar code is scanned and:  
	- if the product is found in products database then it's name and price is printed on LCD display  
	- if the product is not found then error messages: "Product not found" is printed on LCD  
	- if the code scanned is empty then error message "Invalid bar-code" is printed on LCD display  
- when 'exit' is input then receipt is printed on printer containing a list of all previously scanned items names aand prices as well as total sum to be paid for all items; the total sum is also printed on LCD display  

Rules:  
- don't use libraries from io.spring  
- mock/stub the database and IO devices  
- concentrate on proper design and clean coder, rather than supplying fully functioning application  

---------------------------------------------------------------------
To build project from the terminal execute command in root dir:

./gradlew clean build  
java -jar ./build/libs/pos-1.0.jar  

The best way to build project is import project to IntelliJ Idea.

---------------------------------------------------------------------
Product codes in stub DB:  
- chleb  
- mleko  
- maka  
- woda  
- kurczak  
- baton  
